<?php
	
interface iDB {
    /**
     * @param string $host Adres DB
     * @param string $login Login u�ytkownika
     * @param string $password Has�o u�ytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName);

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy si� uda�o wykona� zapytanie czy nie
     */
    public function query($query);

    /**
     * Zwraca liczb� wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}

class DB implements iDB
{
    private $link;
    private $queryResult;
    
    /**
     * @param string $host Adres DB
     * @param string $login Login u�ytkownika
     * @param string $password Has�o u�ytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * 
     * @param string $query Zapytanie do wykonania
     * 
     * @return bool Czy si� uda�o wykona� zapytanie czy nie
     */
    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);
        
        return (bool)$this->queryResult;
    }

    /**
     * Zwraca liczb� wierszy zmodyfikowanych przez ostatnie zapytanie
     * 
     * @return int
     */
    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getRow()
    {
        if (!$this->queryResult) {
         throw new Exception('Zapytanie nie zostalo wykonane');
	   // printf("Errorcode: %d\n", $mysqli->errno);
        }
        
        return mysqli_fetch_assoc($this->queryResult);
    }

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getAllRows()
    {
        $results = array();
        
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        
        return $results;
    }
}

$db = new DB('localhost', '21666679_camp', 'K3rb%+f;ujus', '21666679_camp');

//echo 'Success... ' . $db->host_info . "\n";

$price=$_GET[price];
$name=$_GET[name];
$id=$_GET[product];

if(isset($_GET[product])){
	if(isset($_GET[action]) && $_GET[action]==checkProduct){
		
		$db->query("SELECT * FROM `products` WHERE `id`='$id'");
		
		var_dump($db->getRow());
	}
	
	elseif(isset($_GET[action]) && $_GET[action]==removeProduct){
		
		$db->query("DELETE FROM `products` WHERE `id`='$id'");
		
		$db->query("SELECT * FROM `products`");
		
		
		var_dump($db->getAllRows());
		

		
		
	}
	
}

elseif(!isset($_GET[product])){
 if(isset($_GET[action]) && $_GET[action]==addProduct){
		
		
		
		$db->query("INSERT INTO `products` SET `name`='$name', `price`='$price'");
		
		$db->query("SELECT * FROM `products`");
		
		//var_dump($db->getAllRows());
		
	header("Content-type: text/xml");	
		
			$list=$db->getAllRows();
		
			$simplexml = new SimpleXmlElement('<?xml version="1.0"?><list />');
			
			foreach ($list as $item){
				$simplexml->addChild('item',$item['name']);
			}
			echo $simplexml->asXML();
}
	}



