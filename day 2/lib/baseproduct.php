<?php

require_once('virtualproduct.php');
require_once('product.php');

abstract class BaseProduct{
	
	protected $name;
	protected $price;
	protected $currency;
	protected $id;
	protected $description;
	protected $img;
	protected $producent;
	protected $category;
	protected $quantity;
	
	public function setName($n) { $this->name = $n; }
    public function setPrice($p) { $this->price = $p; }
    public function setCurrency($c) { $this->currency = $c; }
    public function setId($i) { $this->id = $i; }
    public function setDescription($d) { $this->description= $d; }
    public function setImg($im) { $this->img= $im; }
    public function setProducent($pr) { $this->producent= $pr; }
    public function setCategory($cat) { $this->category= $cat; }
  	public function setQuantity($q) { $this->quantity = $q; }

	public function setWeight($weight) { $this->weight= $weight; }
	
	public function getName() { return $this->name; }
    public function getPrice() { return $this->price; }
    public function getCurrency() { return $this->currency; }
    public function getId() { return $this->id; }
    public function getDescription() { return $this->description; }
    public function getimg() { return $this->img; }
    public function getProducent() { return $this->producent; }
    public function getCategory() { return $this->category; }
    public function getQuantity() { return $this->quantity; }	

	public function getWeight() { return $this->weight; }
	   
	   
	  public function __set($name, $value){
		  
		  $this->$name=$value;
		  return $this;
	  }
	    public function __get($name){
		  
		  
		  return $this->$name;
	  }
	  
	  
	  
	 public function __call($name, $arguments) {
        if (preg_match('~^(set|get)([A-Z])(.*)$~', $name, $matches)) {
            $property = strtolower($matches[2]) . $matches[3];
            if (!property_exists($this, $property)) {
                throw new Exception('Property ' . $property . ' not exists');
            }
            switch($matches[1]) {
                case 'set':
                    $this->checkArguments($arguments, 1, 1, $name);
                    return $this->set($property, $arguments[0]);
                case 'get':
                    $this->checkArguments($arguments, 0, 0, $name);
                    return $this->get($property);
                case 'default':
                    throw new Exception('Method ' . $name . ' not exists');
            }
        }
}

	public function set($property, $value) {
        $this->$property = $value;
        return $this;
    }
}






